﻿function New-HybridUser{
<#
.SYNOPSIS
New-HybridUser create a user ready for office 365 in hybrid configuration.
.DESCRIPTION
 This Script is intended to create a user ready for office 365 with all required attribute using ineractive session
 The Proccess let you choose between 2 option ,copy from an existing user of create a new user from scratch 
 After you created the user ,you have the option to license the user with office365.
 In the end of the proceess the user is fully configured in office 365 enviorment.
 .PARAMETER SourceUserName
 The UserName from which you want to copy from

 .PARAMETER TargetUserName
 The UserName for the New User you want to create.

 .PARAMETER FirstName
 New user GivenName.

 .PARAMETER LastName
 New user SureName.

 .PARAMETER Country
 New User Country name. 

 .PARAMETER FullName
 New User Full Name .

 .PARAMETER UpnAddress
 New User UPN Address.
  
 .PARAMETER UserPassword
 New User password.

 .PARAMETER DirSyncServer
 Name of local DirSync Server.

 .EXAMPLE
 New-HybridUser -SourceUserName MaryJ -TargetUserName JackW -FirstName "Jack" -LastName "White" -Country "USA" -FullName "Jack White" -UpnAddress "JackW@contoso.com" -UserPassword P@ssword!3 -DirSyncServer "server-dirsync1"
 
 this command create a new user "Jack White" from an existing AD user "MaryJ" with the same groups and OU location ,then create a mailbox for newly created user in the on-premises exchange server, sync the new object changes to office 365
 when changes are synced ,the command then connects to microsoft online services, assing a user office 365 license and migrate his mailbox to exchange online, 

 
 .EXAMPLE
 New-HybridUser -TargetUserName JackW -FirstName "Jack" -LastName "White" -Country "USA" -FullName "Jack White" -UpnAddress "JackW@contoso.com" -UserPassword P@ssword!3 -DirSyncServer "server-dirsync1"
 
 this command create a new user "Jack White" ,then create a mailbox for newly created user in the on-premises exchange server, sync the new object changes to office 365
 when changes are synced ,the command then connects to microsoft online services, assing a user office 365 license and migrate his mailbox to exchange online, 



 .NOTES
 1.Active Directory Powershell module
 2.Office 365 sign-in assistant - http://www.microsoft.com/en-us/download/details.aspx?id=28177
 3.Windows Azure Active Directory Powershell module - http://technet.microsoft.com/library/jj151815.aspx
 4.DirSync
 5.run the script with at least an AD user that has rights to created and manage users
 6.have credentials of an office 365 global admin
 7.Set-ExecutionPolicy should be set to unrestricted        
 #>
 
  [CmdletBinding()]
 param(

 [String]
 $SourceUserName,

 
 [Parameter(Mandatory = $true)]
 [String]
 $TargetUserName,
 
 
 [Parameter(Mandatory = $true)]
 [String]
 $FirstName,

 
 [Parameter(Mandatory = $true)]
 [String]
 $LastName,
 
 [Parameter(Mandatory = $true)]
 [String]
 $CountryCode,

 [Parameter(Mandatory = $true)]
 [String]
 $UpnAddress,



 [Parameter(Mandatory = $true)]
 [String]
 $DirSyncServer,

 
 [Parameter(Mandatory = $true)]
 [String]
 $MRS,


 [String]
 $ExchangeServer,

 
 [Parameter(Mandatory = $true)]
 [System.Security.Securestring]
 $UserPassword


 )#end param

Write-Host 'Loading Active Directory Module.Please wait...' -ForegroundColor Magenta

Import-Module -Name Activedirectory -WarningAction SilentlyContinue

if($SourceUserName)
{

###Create a new user based on a current user profile###

New-ADUser -SamAccountName $TargetUserName -Instance $SourceUserName -Enabled $true `
-UserPrincipalName $UpnAddress -GivenName ($FirstName) -Surname ($LastName) `

 -Name ($FirstName +' ' + $LastName) -DisplayName ($FirstName +' ' + $LastName)`

  -AccountPassword $UserPassword 

##Copy Selected user Groups to the new user you just created### 

Get-ADUser -Identity $SourceUserName -Properties memberof |Select-Object -ExpandProperty memberof | Add-ADGroupMember -Members $TargetUserName 

Start-Sleep -Seconds 2

$u = Get-ADUser -Identity $SourceUserName 
$DN = $U.DistinguishedName 
$path = $DN.Substring($dn.indexof(',') + 1)

Get-ADUser -Identity $TargetUserName | Move-ADObject -TargetPath $path

}
else
{
New-ADUser -SamAccountName $SourceUserName -Enabled $true `
-UserPrincipalName $UpnAddress -GivenName ($FirstName) -Surname ($LastName) `

 -Name ($FirstName +' ' + $LastName) -DisplayName ($FirstName +' ' + $LastName)`

  -AccountPassword $UserPassword 

}

### Connect to On-Premisis exchnage server to create a mailbox to the new user ###

$Session1 = New-PSSession -ConfigurationName Microsoft.Exchange -ConnectionUri http://$ExchangeServer/PowerShell/ -Authentication Kerberos -WarningAction SilentlyContinue

Import-PSSession $Session1 -AllowClobber | Out-Null -WarningAction SilentlyContinue

Enable-Mailbox -Identity $TargetUserName | Out-Null -WarningAction SilentlyContinue

Remove-PSSession $Session1



$DirsyncAction = Invoke-Command -ComputerName $DirSyncServer -ScriptBlock {C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe -psconsolefile 'C:\Program Files\Windows Azure Active Directory Sync\DirSyncConfigShell.psc1' -command 'Start-OnlineCoexistenceSync'} -Verbose 
Write-Host 'Please wait while the user is being synchronized to the cloud...' -ForegroundColor Yellow

Start-Sleep -Seconds 35


###Connect to Microsoft Online###
$LiveCred = Get-Credential -Message 'Please Enter office 365 Global Admin Credentials'
Write-Host 'Connecting to office 365 online services.Please wait...' -ForegroundColor Magenta

$DirsyncAction = Invoke-Command -ComputerName $DirSyncServer -ScriptBlock {C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe -psconsolefile 'C:\Program Files\Windows Azure Active Directory Sync\DirSyncConfigShell.psc1' -command 'Start-OnlineCoexistenceSync'} -Verbose 
Write-Host 'Please wait while the user is being synchronized to the cloud...' -ForegroundColor Yellow

Start-Sleep -Seconds 45


###Connect to Microsoft Online###
$LiveCred = Get-Credential -Message 'Please Enter office 365 Global Admin Credentials'
Write-Host 'Connecting to office 365 online services.Please wait...' -ForegroundColor Magenta

$Session = New-PSSession -ConfigurationName Microsoft.Exchange -ConnectionUri https://ps.outlook.com/powershell/ -Credential $LiveCred -Authentication Basic -AllowRedirection -WarningAction SilentlyContinue
Import-PSSession $Session -AllowClobber | Out-Null -WarningAction SilentlyContinue
Import-Module -Name MSOnline
Connect-MsolService -Credential $LiveCred


### Getting the parameter for your tenant ###

$OnlineID = Get-MsolAccountSku | Where-Object {$_.SkuPartNumber -eq 'ENTERPRISEPACK'} | Select-Object AccountSkuId -ExpandProperty AccountSkuId

$TargetAddress = Get-MsolDomain |Where-Object {$_.name -cmatch 'mail.onmicrosoft.com'} |Select-Object Name -ExpandProperty name
 


$TargetAddress = Get-MsolDomain |Where-Object {$_.name -cmatch 'mail.onmicrosoft.com'} |Select-Object Name -ExpandProperty name
 




 
### Check for availble license and assign it to the user ### 
$LicenseCheck =  Get-MsolAccountSku | Where-Object {($_.SkuPartNumber -eq 'ENTERPRISEPACK') -and ($_.ConsumedUnits -lt $_.ActiveUnits) }

  if ($LicenseCheck = $true){
  Set-MsolUser -UserPrincipalName $UpnAddress -UsageLocation $CountryCode -BlockCredential $false
  Get-MsolUser -UserPrincipalName $UpnAddress |Set-MsolUserLicense -AddLicenses $OnlineID

Start-Sleep -Seconds 10


###Migrate User mailbox to the office 365#

New-MoveRequest -Remote -RemoteHostName $MRS -Identity $TargetUserName -TargetDeliveryDomain $TargetAddress -RemoteCredential (Get-Credential -Message 'enter domain admin credentials like domain\username') -BatchName $TargetUserName


  ###Exit Office online session###
  Remove-PSSession $Session

  Write-Host -Object "You Have Succesfully created $UpnAddress ,it might take a while for the user mailbox to be migrated to office 365 " -ForegroundColor Green
  }

 else{
 Write-host "You have reached the maximum number of license `nyou cannot add any more licenses n`Please contact your Tenent Admin" -ForegroundColor Yellow
}


 }

function New-OnlineUser{
<#
.SYNOPSIS
New-OnlineUser create a user ready for office 365.
.DESCRIPTION
 This Script is intended to create a user ready for office 365 with all required attribute using ineractive session
 The Proccess let you choose between 2 option ,copy from an existing user of create a new user from scratch 
 After you created the user ,you have the option to license the user with office365.
 In the end of the proceess the user is fully configured in office 365 enviorment.
 .PARAMETER SourceUserName
 The UserName from which you want to copy from

 .PARAMETER TargetUserName
 The UserName for the New User you want to create.

 .PARAMETER FirstName
 New user GivenName.

 .PARAMETER LastName
 New user SureName.

 .PARAMETER Country
 New User Country name. 

 .PARAMETER FullName
 New User Full Name .

 .PARAMETER UpnAddress
 New User UPN Address.
  
 .PARAMETER UserPassword
 New User password.

 .PARAMETER DirSyncServer
 Name of local DirSync Server.

 .EXAMPLE
 New-OnlineUser -SourceUserName MaryJ -TargetUserName JackW -FirstName "Jack" -LastName "White" -Country "USA" -FullName "Jack White" -UpnAddress "JackW@contoso.com" -UserPassword P@ssword!3 -DirSyncServer "server-dirsync1"
 
 this command create a new user "Jack White", sync the new object changes to office 365
 when changes are synced ,the command then connects to microsoft online services, assing a user office 365 license and create a mailbox in exchange online, 

 .NOTES
 1.Active Directory Powershell module
 2.Office 365 sign-in assistant - http://www.microsoft.com/en-us/download/details.aspx?id=28177
 3.Windows Azure Active Directory Powershell module - http://technet.microsoft.com/library/jj151815.aspx
 4.DirSync
 5.run the script with at least an AD user that has rights to created and manage users
 6.have credentials of an office 365 global admin
 7.Set-ExecutionPolicy should be set to unrestricted
 
 #>
 
  [CmdletBinding()]
 param(

 [Parameter(Mandatory = $true)]
 [String]
 $SourceUserName,

 
 [Parameter(Mandatory = $true)]
 [String]
 $TargetUserName,
 
 
 [Parameter(Mandatory = $true)]
 [String]
 $FirstName,

 
 [Parameter(Mandatory = $true)]
 [String]
 $LastName,
 
 [Parameter(Mandatory = $true)]
 [String]
 $CountryCode,

 [Parameter(Mandatory = $true)]
 [String]
 $UpnAddress,

 [Parameter(Mandatory = $true)]
 [System.Security.Securestring]
 $UserPassword,

 
 [Parameter(Mandatory = $true)]
 [String]
 $DirSyncServer


 )#end param


Write-Host 'Loading Active Directory Module.Please wait...' -ForegroundColor Magenta

Import-Module -Name Activedirectory -WarningAction SilentlyContinue

###Create a new user based on a current user profile###
New-ADUser -SamAccountName $TargetUserName -Instance $SourceUserName -Enabled $true `
-UserPrincipalName $UpnAddress -GivenName ($FirstName) -Surname ($LastName) `
-Name ($FirstName +' ' + $LastName) -DisplayName ($FirstName +' ' + $LastName)`

  -AccountPassword $UserPassword -OtherAttributes @{'mail'=$UpnAddress}

##Copy Selected user Groups to the new user you just created### 

Get-ADUser -Identity $SourceUserName -Properties memberof |Select-Object -ExpandProperty memberof | Add-ADGroupMember -Members $TargetUserName 

Start-Sleep -Seconds 2

$u = Get-ADUser -Identity $SourceUserName 
$DN = $U.DistinguishedName 
$path = $DN.Substring($dn.indexof(',') + 1)

Get-ADUser -Identity $TargetUserName | Move-ADObject -TargetPath $path


$DirsyncAction = Invoke-Command -ComputerName $DirSyncServer -ScriptBlock {C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe -psconsolefile 'C:\Program Files\Windows Azure Active Directory Sync\DirSyncConfigShell.psc1' -command 'Start-OnlineCoexistenceSync'} -Verbose 
$DirsyncAction
Write-Host 'Please wait while the user is being synchronized to the cloud...' -ForegroundColor Yellow

Start-Sleep -Seconds 45

### Connect to office 365 ###

Import-Module -Name MSOnline -WarningAction SilentlyContinue

Write-Host 'Loading Microsoft Online Module.Please wait...' -ForegroundColor Magenta
Write-Host 'Enter Office365 Global Admin Credentials' -ForegroundColor Cyan 

Connect-MsolService

### Getting the Lisence detailes for your tenant ###

$license = Get-MsolAccountSku | Where-Object {$_.SkuPartNumber -eq 'ENTERPRISEPACK'} | Select-Object AccountSkuId -ExpandProperty AccountSkuId
 
### Check for availble lisence and assign it to the user ### 
$LicenseCheck =  Get-MsolAccountSku | Where-Object {($_.SkuPartNumber -eq 'ENTERPRISEPACK') -and ($_.ConsumedUnits -lt $_.ActiveUnits) }

  if ($LicenseCheck = $true){
  Set-MsolUser -UserPrincipalName $UpnAddress -UsageLocation $CountryCode -BlockCredential $false
  Get-MsolUser -UserPrincipalName $UpnAddress |Set-MsolUserLicense -AddLicenses $license 

  ### Sets user proxy and target addresses ###

  $TargetAddress = Get-MsolDomain |Where-Object {$_.name -cmatch 'mail.onmicrosoft.com'} |Select-Object Name -ExpandProperty name
  Set-ADUser -Identity $TargetUserName -Add @{proxyAddresses="smtp:$TargetUserName@$targetaddress","SMTP:$UpnAddress";TargetAddress="$TargetUserName@$targetaddress"}
  Write-Host -Object 'Wait while user is being proccessed for other attributes'

  $DirsyncAction
  Start-Sleep -Seconds 20

  Write-Host -Object "You Have Succesfully craeted $UpnAddress" -ForegroundColor Green
  }

 else{
 Write-host "You have reached the maximum number of license `nyou cannot add any more licenses n`Please contact your Tenent Admin" -ForegroundColor Yellow
}


 }
function Get-HybridUser{
<#
.SYNOPSIS
Get-HybridUser get user detailes on office 365 in hybrid configuration.
.DESCRIPTION
 This Script is design to get Microsoft online user mailbox and license information, focusing on the real world requirements .
 .PARAMETER UserName
 The UserName you want to query

 .PARAMETER MailboxInfo
 The type of maibox information you want to retrieve.
 Option:
 1.statics = mailbox general information
 2.id = mailbox size information
 3.migration -user migration status

 .PARAMETER LicensedUser 
 information about user license status.

 .EXAMPLE
 Get-HybridUser -Username BillG -MailboxInfo migration

 this command gets a migration status for the user BillG
 
 .EXAMPLE
 Get-HybridUser -Username BillG -MailboxInfo statics

 this command get full mailbox statics for a user BillG


 .EXAMPLE
 Get-HybridUser -Username BillG -LicensedUser BillG@microsoft.com

 this command get the license inforamtion about the user BillG

  .NOTES
 1.Office 365 sign-in assistant - http://www.microsoft.com/en-us/download/details.aspx?id=28177
 2.Windows Azure Active Directory Powershell module - http://technet.microsoft.com/library/jj151815.aspx
 3.have credentials of an office 365 global admin
 4.Set-ExecutionPolicy should be set to unrestricted
#>

[cmdletbinding()]

 param (
 

 [Parameter(Mandatory = $False)]
           [System.Management.Automation.PsCredential]$Credential,

 [Parameter(Mandatory = $true)]
 [string] $Username,

[Parameter(ValueFromPipelineByPropertyName=$true)]
[String]$MailboxInfo,


[string]
$LicensedUser 


 )


 while((Get-PSSession).ConfigurationName -ne 'Microsoft.Exchange')

    { 
 
$Credential = Get-Credential -Message 'Enter the username and password of an Exchange Online Global Administrator account.'

Write-Host 'Connecting to office 365 online services.Please wait...' -ForegroundColor Magenta


$Session = New-PSSession -ConfigurationName Microsoft.Exchange -ConnectionUri https://ps.outlook.com/powershell/ -Credential $Credential -Authentication Basic -AllowRedirection -WarningAction SilentlyContinue

Import-PSSession $Session -AllowClobber | Out-Null -WarningAction SilentlyContinue

Import-Module -Name MSOnline

Connect-MsolService -Credential $Credential

    
    Break
    }

if($LicensedUser -and !$MailboxInfo){


 Get-MsolUser -UserPrincipalName $LicensedUser | Select-Object UserPrincipalName, IsLicensed, Licenses | Format-List

 return
    
    }

elseif($LicensedUser -and $MailboxInfo){


    Get-MsolUser -UserPrincipalName $LicensedUser | Select-Object UserPrincipalName, IsLicensed, Licenses | Format-List

    }

foreach($user in $Username)

    {

switch($MailboxInfo){
 
 statics{Get-MailboxStatistics -Identity $Username}

 id{Get-MailboxStatistics -Identity $UserName | Select-Object DisplayName, TotalItemSize, DatabaseProhibitSendReceiveQuota | Format-Table Displayname, @{label='MailboxSize';Expression={$_.TotalItemSize}},@{label='Mailbox Limit Size';Expression={$_.DatabaseProhibitSendReceiveQuota}} }
    
 migration  { Get-MoveRequestStatistics -Identity $UserName  | Format-List  DisplayName,Status,QueuedTimestamp,StartTimestamp,FinalSyncTimestamp,CompletionTimestamp,OverallDuration }

 default { Get-MailboxStatistics -Identity $Username | Format-List }

        }
      }
    }
    