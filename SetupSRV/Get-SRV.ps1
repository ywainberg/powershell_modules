﻿function Get-SRV{

  <#
      .SYNOPSIS
      Get server initial settings

      .DESCRIPTION
      Get server initial setting including ip settings and disks

      .PARAMETER ComputerName
      set the desired name for the host

      .PARAMETER DiskSetting
      Get current volumes info

      .PARAMETER NetSetting
      Get IP configuration settings

      .PARAMETER WORKGROUP
      Set different crederntilas for action

      .PARAMETER System
      Get operating system info 

      .EXAMPLE
      Get-SRV -ComputerName 'MyServerName' -NetSetting 
 
      This command will retrive the network configuration for the computer 'MyServerName' .

       .EXAMPLE
      Get-SRV -ComputerName 'MyServerName' -System 
 
      This command will retrive the system information for the computer 'MyServerName' .

      .EXAMPLE
      Get-SRV -ComputerName 'MyServerName' -DiskSetting

      This command will get the volume configuration for the computer 'MyServerName'  .

     
      .NOTES
      + Require PS v.4 or up
      + Script Exection Policy should be unrestricted
     
  #>



  [CmdLetBinding()]

  param(

    [Parameter(Mandatory = $true, Position = 0,HelpMessage='Enter one or more computer names separated by commas.')]
    [ValidateNotNullOrEmpty()]
    [string[]]
    $ComputerName = $env:COMPUTERNAME,

    [Parameter(Mandatory=$false)]
    [switch]
    $NetSetting,

    [Parameter(Mandatory=$false)]
    [switch]
    $DiskSetting,
    
    
    [Parameter(Mandatory=$false)]
    [switch]
    $WORKGROUP,

    [Parameter(Mandatory=$false)]
    [switch]
    $System
     )
     
     if($WORKGROUP){ 
       $session = New-CimSession -ComputerName $ComputerName -Credential(Get-Credential)
     }
     
     else{

            $session = New-CimSession -ComputerName $ComputerName 
     }
     
  if($NetSetting){

    $InterfaceAlias = 'Ethernet'
    $hostip = Get-NetIPConfiguration -CimSession $session -InterfaceAlias $InterfaceAlias 
    $host_address = ($hostip.IPv4Address).IPAddress
    $host_dg = ($hostip.IPv4DefaultGateway).nexthop
    $host_dns = ($hostip.DNSServer).ServerAddresses
    $dhcp = (Get-CimInstance Win32_NetworkAdapterConfiguration -Property * -CimSession $session | Where-Object{$_.IPAddress -eq $host_address}).DHCPEnabled
  

    $COMPUTER = New-Object psobject

    

          $COMPUTER | Add-Member NoteProperty  'ip address' ($host_address)
          $COMPUTER | Add-Member NoteProperty 'Default Gateway' ($host_dg)
          $COMPUTER | Add-Member NoteProperty 'DNS Servers' ($host_dns)
          $COMPUTER | Add-Member NoteProperty 'Adapter Name' ($InterfaceAlias)
          $COMPUTER | Add-Member NoteProperty 'DHCP Enabled' ($dhcp)

    $COMPUTER
           
           
           }

    if($DiskSetting){
    
        $sizing = Get-Volume -CimSession $session |Where-Object{($_.DriveType -like 'fixed') -and ($_.FileSystemLabel -ne 'System Reserved')}
        
        
        foreach($size in $sizing ){
        
          $Remain =  [math]::Round($size.SizeRemaining/ 1gb)
          $current =  [math]::Round($size.Size/ 1gb)
          
        
 
          $disk_info = New-Object psobject
          
      
          $disk_info | Add-Member NoteProperty 'Drive Letter' ($size.DriveLetter)
          $disk_info | Add-Member NoteProperty 'Used Space(GB)' ($current)
          $disk_info | Add-Member NoteProperty 'Free Space(GB)' ($Remain)
      
          $disk_info
      
        }
          
    
    }
    
    if($System){
  
      
      $platform =  (Get-CimInstance win32_computersystem -CimSession $session).partofdomain 
      $architecture = (Get-CimInstance Win32_OperatingSystem -CimSession $session |Select-Object OSArchitecture).OSArchitecture
      $windows_version = ((Get-CimInstance Win32_OperatingSystem -CimSession $session).Name).split('|')[0]
      $memory = [math]::Round(((Get-CimInstance Win32_PhysicalMemory -CimSession $session | Measure-Object -Property capacity -Sum).Sum / 1gb))
      $processor = (Get-CimInstance Win32_ComputerSystem -CimSession $session).NumberOfLogicalProcessors
      
      $mysystem = New-Object psobject
      
      $mysystem |Add-Member NoteProperty 'IsDomainJoint' ($platform)
      $mysystem |Add-Member NoteProperty 'OS Platform' ($architecture)
      $mysystem |Add-Member NoteProperty 'OS Version' ($windows_version)
      $mysystem |Add-Member NoteProperty 'Physical Memory(GB)' ($memory)
      $mysystem |Add-Member NoteProperty 'logical Processors' ($processor)
      
    
      $mysystem
    }
  
}