﻿<#	
	===========================================================================
	 Created with: 	SAPIEN Technologies, Inc., PowerShell Studio 2016 v5.2.127
	 Created on:   	24/08/2016 17:15
	 Created by:   	yaronw
	 Organization: 	example
	 Filename:     	User365.psd1
	 -------------------------------------------------------------------------
	 Module Manifest
	-------------------------------------------------------------------------
	 Module Name: User365
	===========================================================================
#>

@{

# Script module or binary module file associated with this manifest
RootModule = 'User365.psm1'

# Version number of this module.
ModuleVersion = '1.0.0.10'

# ID used to uniquely identify this module
GUID = '0440529b-36e8-457c-b688-7426df2ce0b7'

# Author of this module
Author = 'yaronw'

# Company or vendor of this module
CompanyName = 'example'

# Copyright statement for this module
Copyright = '(c) 2016. All rights reserved.'

# Description of the functionality provided by this module
Description = 'Create new user in office 365 while working in hybrid AD'

# Minimum version of the Windows PowerShell engine required by this module
PowerShellVersion = '2.0'

# Name of the Windows PowerShell host required by this module
PowerShellHostName = ''

# Minimum version of the Windows PowerShell host required by this module
PowerShellHostVersion = '3.0'

# Minimum version of the .NET Framework required by this module
DotNetFrameworkVersion = '2.0'

# Minimum version of the common language runtime (CLR) required by this module
CLRVersion = '2.0.50727'

# Processor architecture (None, X86, Amd64, IA64) required by this module
ProcessorArchitecture = 'None'

# Modules that must be imported into the global environment prior to importing
# this module
RequiredModules = @('MSOnline')

# Assemblies that must be loaded prior to importing this module
RequiredAssemblies = @()

# Script files (.ps1) that are run in the caller's environment prior to
# importing this module
ScriptsToProcess = @()

# Type files (.ps1xml) to be loaded when importing this module
TypesToProcess = @()

# Format files (.ps1xml) to be loaded when importing this module
FormatsToProcess = @()

# Modules to import as nested modules of the module specified in
# ModuleToProcess
NestedModules = @()

# Functions to export from this module
FunctionsToExport = '*'

# Cmdlets to export from this module
CmdletsToExport = '*'

# Variables to export from this module
VariablesToExport = '*'

# Aliases to export from this module
AliasesToExport = '*'

# List of all modules packaged with this module
ModuleList = @("AADsync")

# List of all files packaged with this module
FileList = @('Country_code.json')

# Private data to pass to the module specified in ModuleToProcess
PrivateData = ''

}







