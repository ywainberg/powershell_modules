﻿<#	
	===========================================================================
	 Created with: 	SAPIEN Technologies, Inc., PowerShell Studio 2016 v5.2.127
	 Created on:   	24/08/2016 17:15
	 Created by:   	yaronw
	 Organization: 	example
	 Filename:     	User365.psm1
	-------------------------------------------------------------------------
	 Module Name: User365
	===========================================================================
#>

# Json file that contain country names and codes
$pspath = $env:PSModulePath.Split(';')
$json = foreach ($p in $pspath) { Get-ChildItem -Path $p -Recurse -Filter *.json -ErrorAction SilentlyContinue| where{ $_.name -eq 'Country_code.json' } | select -ExpandProperty directoryname }
$codes = ((Get-Content $json\Country_code.json) -join "`n" | ConvertFrom-Json)



function Set-OnlineLicense {
	
	[CmdletBinding()]
	param
	(
		
		[Parameter(Mandatory = $true,HelpMessage = 'Enter user upn address')]
		[string]$UPN,
		
		[Parameter(Mandatory = $true)]
		[ValidateSet('E1', 'E3', 'E4')]
		[string]$LicenseType
		
		
	)
	
	DynamicParam
	{
		
		# Set the dynamic parameters name
		$ParameterName = 'Country'
		
		# Create the dictionary
		$RuntimeParameterDictionary = New-Object System.Management.Automation.RuntimeDefinedParameterDictionary
		
		# Create the collection of attributes
		$AttributeCollection = New-Object System.Collections.ObjectModel.Collection[System.Attribute]
		
		# Create and set the parameters' attributes
		$ParameterAttribute = New-Object System.Management.Automation.ParameterAttribute
		$ParameterAttribute.ValueFromPipeline = $true
		$ParameterAttribute.ValueFromPipelineByPropertyName = $true
		$ParameterAttribute.Mandatory = $true
		#$ParameterAttribute.Position = 0
		
		# Add the attributes to the attributes collection
		$AttributeCollection.Add($ParameterAttribute)
		
		# Generate and set the ValidateSet
		$arrSet = $codes.name #[Enum]::GetNames('System.Environment+SpecialFolder')
		$ValidateSetAttribute = New-Object System.Management.Automation.ValidateSetAttribute($arrSet)
		
		# Add the ValidateSet to the attributes collection
		$AttributeCollection.Add($ValidateSetAttribute)
		
		# Create and return the dynamic parameter
		$RuntimeParameter = New-Object System.Management.Automation.RuntimeDefinedParameter($ParameterName, [string], $AttributeCollection)
		$RuntimeParameterDictionary.Add($ParameterName, $RuntimeParameter)
		return $RuntimeParameterDictionary
		
		
	}
	Process
	{
		# Bind the parameter to a friendly variable
		$Country = $PsBoundParameters[$ParameterName]
		
		
		Import-Module -Name MSOnline
		Write-Host "Loading Microsoft Online Module.Please wait..." -ForegroundColor Magenta
		Write-Host "Enter Office365 Global Admin Credentials" -ForegroundColor Cyan
		$cred = (Get-Credential) 
		Connect-MsolService -Credential $cred 
		
		
		
		
		switch ($LicenseType)
		{
			'E1' {
				### Check for availble lisence and assign it to the user ### 
				$LicenseCheck = Get-MsolAccountSku | Where-Object { ($_.SkuPartNumber -eq 'STANDARDPACK') -and ($_.ConsumedUnits -lt $_.ActiveUnits) }
				
				
				
				if ($LicenseCheck)
				{
					$license = Get-MsolAccountSku | Where-Object { $_.SkuPartNumber -eq 'STANDARDPACK' } | Select-Object AccountSkuId -ExpandProperty AccountSkuId
					
				}
				else
				{
					throw "There was a problem with the assigning a license to `n $UPN, `n please check for availble licenses "
				}
				
			}
			'E3' {
				
				$LicenseCheck = Get-MsolAccountSku | Where-Object { ($_.SkuPartNumber -eq 'ENTERPRISEPACK') -and ($_.ConsumedUnits -lt $_.ActiveUnits) }
				
				if ($LicenseCheck)
				{
					$license = Get-MsolAccountSku | Where-Object { $_.SkuPartNumber -eq 'ENTERPRISEPACK' } | Select-Object AccountSkuId -ExpandProperty AccountSkuId
					
				}
				else
				{
					throw "There was a problem with the assigning a license to `n $UPN, `n please check for availble licenses "
				}
			}
			
			'E4' {
				$LicenseCheck = Get-MsolAccountSku | Where-Object { ($_.SkuPartNumber -eq 'ENTERPRISEWITHSCAL') -and ($_.ConsumedUnits -lt $_.ActiveUnits) }
				
				if ($LicenseCheck)
				{
					$license = Get-MsolAccountSku | Where-Object { $_.SkuPartNumber -eq 'ENTERPRISEWITHSCAL' } | Select-Object AccountSkuId -ExpandProperty AccountSkuId
					
				}
				else
				{
					throw "There was a problem with the assigning a license to `n $UPN, `n please check for availble licenses "
				}
			}
			
			default
			{
				$LicenseCheck = Get-MsolAccountSku | Where-Object { ($_.SkuPartNumber -eq 'ENTERPRISEWITHSCAL') -and ($_.ConsumedUnits -lt $_.ActiveUnits) }
				
				if ($LicenseCheck)
				{
					$license = Get-MsolAccountSku | Where-Object { $_.SkuPartNumber -eq 'ENTERPRISEWITHSCAL' } | Select-Object AccountSkuId -ExpandProperty AccountSkuId
					
				}
				else
				{
					throw "There was a problem with the assigning a license to `n $UPN `n please check for availble licenses "
				}
			}
		}
		
	  
		$Country_code = $codes | Where-Object{ $_.Name -eq $Country } | Select-Object -ExpandProperty Code
		
		Get-MsolUser -UserPrincipalName $UPN | Set-MsolUser -UsageLocation $Country_code
		Set-MsolUserLicense -UserPrincipalName $UPN -AddLicenses $license
	
		
		
	}
	
}

function New-OnlineUser
{
	
	
	[CmdletBinding()]
	param (
		
		[Parameter(Mandatory = $true)]
		[String]$SourceUserName,
		
		[Parameter(Mandatory = $true)]
		[String]$FirstName,
		
		[Parameter(Mandatory = $true)]
		[String]$LastName,
		
		[Parameter(Mandatory = $true)]
		[String]$EmailSuffix,
		
		[Parameter()]
		[ValidateSet('3 Month Archive Policy', 'example Retention Policy',`
										'1 Month Archive Policy' ,'6 Month Archive Policy')]
		[string]
		$ArchivePolicy = 'example Retention Policy',
		
		[Parameter(Mandatory = $true)]
		[System.Security.Securestring]$UserPassword,
		
		[Parameter(Mandatory = $true)]
		[String]$DirSyncServer
		
		
	) #end param
	
	
	DynamicParam
	{
		
		# Set the dynamic parameters name
		$ParameterName = 'Country'
		
		# Create the dictionary
		$RuntimeParameterDictionary = New-Object System.Management.Automation.RuntimeDefinedParameterDictionary
		
		# Create the collection of attributes
		$AttributeCollection = New-Object System.Collections.ObjectModel.Collection[System.Attribute]
		
		# Create and set the parameters' attributes
		$ParameterAttribute = New-Object System.Management.Automation.ParameterAttribute
		$ParameterAttribute.ValueFromPipeline = $true
		$ParameterAttribute.ValueFromPipelineByPropertyName = $true
		$ParameterAttribute.Mandatory = $true
		$ParameterAttribute.Position = 0
		
		# Add the attributes to the attributes collection
		$AttributeCollection.Add($ParameterAttribute)
		
		# Generate and set the ValidateSet
		$arrSet = $codes.name  #[Enum]::GetNames('System.Environment+SpecialFolder')
		$ValidateSetAttribute = New-Object System.Management.Automation.ValidateSetAttribute($arrSet)
		
		# Add the ValidateSet to the attributes collection
		$AttributeCollection.Add($ValidateSetAttribute)
		
		# Create and return the dynamic parameter
		$RuntimeParameter = New-Object System.Management.Automation.RuntimeDefinedParameter($ParameterName, [string], $AttributeCollection)
		$RuntimeParameterDictionary.Add($ParameterName, $RuntimeParameter)
		return $RuntimeParameterDictionary
		
		
	}
	Process
	{
		# Bind the parameter to a friendly variable
		$Country = $PsBoundParameters[$ParameterName]
		Write-Verbose -Message 'importing AD module'
		Write-Host "Loading Active Directory Module.Please wait..." -ForegroundColor Magenta
		Import-Module -Name Activedirectory
		
		
		
		###Create a new user based on a current user profile###
		
		$TargetUserName = ($FirstName + $LastName[0]).ToLower()
		$UpnAddress = $TargetUserName + '@' + $EmailSuffix
		$proxyaddress = $EmailSuffix.Split('.')[0]
		$online_address = "$TargetUserName@$EmailSuffix.mail.onmicrosoft.com"

		New-ADUser -SamAccountName $TargetUserName -Instance $SourceUserName -Enabled $true `
							 -UserPrincipalName $UpnAddress -GivenName ($FirstName) -Surname ($LastName) `
							 -Name ($FirstName + " " + $LastName) -DisplayName ($FirstName + " " + $LastName)`
							 -AccountPassword $UserPassword -OtherAttributes @{ 'mail' = $UpnAddress; 'proxyAddresses' = "smtp:$online_address" ; 'TargetAddress' = $online_address; 'msExchRemoteRecipientType' = 3 }
		
		Start-Sleep -Seconds 3			
		Set-ADUser -Identity $TargetUserName -Add @{'proxyAddresses' = "SMTP:$UpnAddress"}

		##Copy Selected user Groups to the new user you just created### 
		
		Get-ADUser -Identity $SourceUserName -Properties memberof | Select-Object -ExpandProperty memberof | Add-ADGroupMember -Members $TargetUserName
		
		Start-Sleep -Seconds 2
		
		$u = Get-ADUser -Identity $SourceUserName
		$DN = $U.DistinguishedName
		$path = $DN.Substring($dn.indexof(',') + 1)
		
		Get-ADUser -Identity $TargetUserName | Move-ADObject -TargetPath $path
		
		#set archive
		Set-ADUser -Identity $TargetUserName -Replace @{msExchRemoteRecipientType = 3}
				
		
		Import-Module AADsync
		start-dirsync -ComputerName $DirSyncServer -SyncType Delta
		
		
		
		Write-Host "Please wait while the user is being synchronized to the cloud..." -ForegroundColor Yellow
		
		Start-Sleep -Seconds 45
		
		### Connect to office 365 ###
		
		
		Import-Module -Name MSOnline
		Write-Host "Loading Microsoft Online Module.Please wait..." -ForegroundColor Magenta
		Write-Host "Enter Office365 Global Admin Credentials" -ForegroundColor Cyan
		$cred = (Get-Credential)
		Connect-MsolService -Credential $cred
		#connect to exchange online
		$exchangeSession = New-PSSession -ConfigurationName Microsoft.Exchange -ConnectionUri "https://outlook.office365.com/powershell-liveid/" -Credential $cred -Authentication "Basic" -AllowRedirection
		Import-PSSession $exchangeSession -DisableNameChecking | Out-Null
		
		
		
		### Getting the Lisence detailes for your tenant ###
		
		$license = Get-MsolAccountSku | Where-Object { $_.SkuPartNumber -eq 'ENTERPRISEWITHSCAL' } | Select-Object AccountSkuId -ExpandProperty AccountSkuId
		
		### Check for availble lisence and assign it to the user ### 
		$LicenseCheck = Get-MsolAccountSku | Where-Object { ($_.SkuPartNumber -eq 'ENTERPRISEWITHSCAL') -and ($_.ConsumedUnits -lt $_.ActiveUnits) }
		$Country_code = $codes | Where-Object{ $_.Name -eq $Country } | Select-Object -ExpandProperty Code
		
		if ($LicenseCheck = $true)
		{
			
			Get-MsolUser -UserPrincipalName $UpnAddress | Set-MsolUser -UsageLocation $Country_code
			Start-Sleep -Seconds 2
			Set-MsolUserLicense -UserPrincipalName $UpnAddress -AddLicenses $license
			
			
			Write-Host -Object "You Have Succesfully craeted $UpnAddress" -ForegroundColor Green
		}
		
		else
		{
			Write-host "You have reached the maximum number of license `nyou cannot add any more licenses n`Please contact your Tenent Admin" -ForegroundColor Yellow
		}
			#enable online archive
			Start-Sleep -Seconds 10
		Get-Mailbox $UpnAddress | Set-Mailbox -RetentionPolicy $ArchivePolicy
		start-dirsync -ComputerName $DirSyncServer -SyncType Delta
	}
}


function Get-OnlineInfo
{
	[CmdletBinding()]
	
	param
	(
		[parameter(ParameterSetName = 'License')]
		[Validateset('Available', 'Consumend')]
		[string]$TenenatLicense,
		
		[Parameter(ValueFromPipeline = $true,ParameterSetName = 'User', ValueFromPipelineByPropertyName = $true)]
		[ValidateSet('ProxyAddress', 'MailboxPermission' ,'Archive')]
		[string]$UserInfo,
		
		[Parameter(ParameterSetName = 'User',ValueFromPipeline = $true)]
		[string[]]$UPN
			
		
	)
	
	#Connect to Online Services
	$LiveCred = Get-Credential -Message 'Enter Global Admin credentials'
	
	Connect-MsolService -Credential $LiveCred
	
	
	
	
	switch ($TenenatLicense) {
		'Available' {
			
			#Get License Info
			$E4 = Get-MsolAccountSku | Where-Object{ $_.AccountSkuId -match 'ENTERPRISEWITHSCAL' }
			$E3 = Get-MsolAccountSku | Where-Object{ $_.AccountSkuId -match 'ENTERPRISEPACK' }
			$E1 = Get-MsolAccountSku | Where-Object{ $_.AccountSkuId -match 'STANDARDPACK' }
			$E = New-Object System.Management.Automation.PSObject
			$E | Add-Member -MemberType NoteProperty -Name 'E4 Availble License' -Value ($E4.ActiveUnits - $E4.ConsumedUnits)
			$E | Add-Member -MemberType NoteProperty -Name 'E3 Availble License' -Value ($E3.ActiveUnits - $E3.ConsumedUnits)
			$E | Add-Member -MemberType NoteProperty -Name 'E1 Availble License' -Value ($E1.ActiveUnits - $E1.ConsumedUnits)
			$E
		}
		'Consumend' {
			#Get License Info
			$E4 = Get-MsolAccountSku | Where-Object{ $_.AccountSkuId -match 'ENTERPRISEWITHSCAL' }
			$E3 = Get-MsolAccountSku | Where-Object{ $_.AccountSkuId -match 'ENTERPRISEPACK' }
			$E1 = Get-MsolAccountSku | Where-Object{ $_.AccountSkuId -match 'STANDARDPACK' }
			$E = New-Object System.Management.Automation.PSObject
			$E | Add-Member -MemberType NoteProperty -Name 'E4 Consumend License' -Value ("$E4.ConsumedUnits out of $E4.ActiveUnits ")
			$E | Add-Member -MemberType NoteProperty -Name 'E3 Consumend License' -Value ("$E3.ConsumedUnits out of $E3.ActiveUnits")
			$E | Add-Member -MemberType NoteProperty -Name 'E1 Consumend License' -Value ("$E1.ConsumedUnits out of $E1.ActiveUnits")
			$E |Format-List
			
			
		}
	
		default {
			
			$E | Add-Member -MemberType NoteProperty -Name 'E4 Availble License' -Value ($E4.ActiveUnits - $E4.ConsumedUnits)
			$E | Add-Member -MemberType NoteProperty -Name 'E3 Availble License' -Value ($E3.ActiveUnits - $E3.ConsumedUnits)
			$E | Add-Member -MemberType NoteProperty -Name 'E1 Availble License' -Value ($E1.ActiveUnits - $E1.ConsumedUnits)
			$E
		}
	}
	
	$exchangeSession = New-PSSession -ConfigurationName Microsoft.Exchange -ConnectionUri "https://outlook.office365.com/powershell-liveid/" -Credential $LiveCred -Authentication "Basic" -AllowRedirection
	Import-PSSession $exchangeSession -DisableNameChecking | Out-Null
	switch ($UserInfo) {
		'ProxyAddress'{
			
			$mail = (Get-Mailbox -Identity $UPN| Select-Object -ExpandProperty EmailAddresses)
			foreach ($m in $mail) {($m).split(':')[1]}
			
			
		}
		'MailboxPermission' {
			$mailboxs = Get-MailboxPermission -User $UPN -Identity *
			$counter = 0
			
			foreach ($m in $mailboxs) {
				
				$counter++
				Write-Progress -Activity 'Proccessing Mailbox list' -CurrentOperation $m -PercentComplete (($counter / $mailboxs.count) * 100)
			}
		}
		'Archive'{
			
			foreach ($m in $UPN) {
				Get-Mailbox $m | Select-Object  RetentionPolicy, ArchiveStatus
			}
			
			
		}
	}
	
	
}


Export-ModuleMember -Function New-OnlineUser
Export-ModuleMember -Function Set-OnlineLicense
Export-ModuleMember -Function Get-OnlineInfo