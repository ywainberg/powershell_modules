﻿Function Get-NestGRP {
<#
.SYNOPSIS
 Get-NestGRP provide information about nested group.
.DESCRIPTION
 This Script let you pull information about nested groups inside a primary group.
 You can get all the memebrs that are nested inside, Get group list and provide additional important information

 .PARAMETER RootGroup
 The primary group you want to query.

 .PARAMETER Output
 The Additional details you want to retrieve.
 Option:
 Users - List all users from all nested group.
 Groups - list all groups name.
 Detailed - list  groups with  advanced properties.



 .EXAMPLE
 Get-NestGRP -RootGroup GRP-All
 List all group members with their object class

 .EXAMPLE
 Get-NestGRP -RootGroup GRP-All -Output Users
 List all users from from all nested group under the RootGroup.

 #>


  [CmdletBinding()]
  param

    (

    [Parameter(mandatory = $true,ValueFromPipeline = $true,Position = 0, `
    HelpMessage = 'Enter Primary Group Name')]

    [string]$RootGroup,

    [Parameter()]
    [string]$output 

   )



  if($RootGroup -and !$output){

    Write-Verbose 'Main Group is Being Proccessed'  
    Get-ADGroupMember -Identity $RootGroup | Format-Table Name,objectClass
  }
  elseif ($output)
  {
 
    Write-Verbose 'Fetching Additional Group Details'

    # Get Primary Group Information
    $Gprimary = Get-ADGroupMember -Identity $RootGroup

    # Extract Nested Groups from Primary Group
    ForEach ($Group in $Gprimary)

    {

   
      switch($output)
       
      {
   

        Users{
     
    
          $grpname = Get-ADGroup -Identity $Group.name 
          foreach ($item in $grpname)
  
          {
            # For List of memebers from all nested groups 
  
  
            $member = (Get-ADGroupMember -Identity $item).Name
          }

          $list = New-Object PSObject 

          $list | Add-Member NoteProperty 'Group Name' ($grpname.Name)
        Write-Output "`n", $list , "`n", $member  }
       

        Groups{
      
         
     
          $Group.Name
         
              }


        Detailed{
       
       

          $grpname = Get-ADGroup -Identity $Group.name
          foreach ($item in $grpname)
  
          {
            $grpinfo =  Get-ADGroup -Identity $item -Properties *| select Name,GroupScope,GroupCategory,Created 
           
        
          }
          # Create coustom table for showing Group details 
          $detail = New-Object PSObject 

          $detail | Add-Member NoteProperty 'Group Name' ($grpinfo.Name)
          $detail | Add-Member NoteProperty 'Group Type' ($grpinfo.GroupCategory)
          $detail | Add-Member NoteProperty 'Group Scope' ($grpinfo.GroupScope)        
          $detail | Add-Member NoteProperty 'Group Create Time' ($grpinfo.Created) 
          $detail 
 
        }  
      }
    }
     

  } 
 
    else {return}

    
    }
 
 
 
 

        
        
        

 
 

 
 

 
